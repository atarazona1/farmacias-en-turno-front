import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'farmacias-en-turno-front';
  filters: any;
  drugstores: Array<any>;
  region: string;
  private url: string;

  farmaForm = new FormGroup({
    region: new FormControl(''),
    neighborhood: new FormControl(''),
    local_name: new FormControl(''),
  });

  constructor(
    private http: HttpClient,
  ) {
    this.drugstores = [];
  }

  ngOnInit() {
    this.url = 'http://localhost:3000/region/';
    this.farmaForm = new FormGroup({
      region: new FormControl('7'),
      neighborhood: new FormControl(''),
      local_name: new FormControl(''),
    });
  }


  buscar() {
    console.log('Buscar: ', this.farmaForm.value);
    this.filters = {
      neighborhood: this.farmaForm.value.neighborhood || null,
      local_name: this.farmaForm.value.local_name || null,
    }

    const fullUrl = this.url + this.farmaForm.value.region;

    this.http.post<any[]>(fullUrl, this.filters).toPromise()
      .then(list => {
        console.log(list);

        this.drugstores = list;
      })
      .catch(err => {
        console.error(err);
      });

  }
}
